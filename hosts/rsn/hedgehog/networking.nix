{...}: {
  networking = {
    hostId = "8425e349";
    wireless = {
      enable = true;
      allowAuxiliaryImperativeNetworks = true;
      userControlled.enable = true;
    };
  };
}
