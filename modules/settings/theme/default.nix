{...}: {
  imports = [
    ./gruvbox-dark
  ];

  soxin.settings.theme = "gruvbox-dark";
}
